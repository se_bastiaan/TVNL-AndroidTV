TVNL for Android TV
=========================

This is the source code of the ['TVNL' Android TV application](https://play.google.com/store/apps/details?id=eu.se_bastiaan.tvnl).

It's been some time since I built the [NPO application for Windows Phone](https://github.com/se-bastiaan/TVNL-WindowsPhone). Now that I wanted to build something using Android's [Leanback library](http://developer.android.com/tools/support-library/features.html#v17-leanback) I decided that an Android TV application had some potential.

The application is not completely done yet, I still need to finish some things. But those are things for the future.

## To-do list

- [ ] Write unit tests
- [ ] Write functional (UI) tests
- [ ] Write integration tests
- [ ] Setup a CI
- [ ] Add documentation

## License

Copyright 2016 Sébastiaan (github.com/se-bastiaan)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.